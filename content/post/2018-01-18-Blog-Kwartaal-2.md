---
Title: Blog Kwartaal 2
Subtitle: Weken 1 tm 8
Date: 2018-01-18
Tags: ["Blog", "Kwartaal2"]
---

## Week 1
Maandag werden de nieuwe teams gemaakt. Uiteindelijk kwam ik in het team bij
Rick, Joshua en Fabian. Fabian en Joshua kende ik al, maar Rick was nieuw voor mij.
We zijn deze week verder gelijk aan de slag gegaan aan het bedenken van een teamnaam en logo.
Op de teamnaam zijn we gekomen door middel van mind maps en door een combinatie van twee dingen
zijn we op de teamnaam Alpha Four gekomen. Rick heeft ook gelijk een tof logo ontworpen.
Daarna kozen we de teamcaptain. Dat is uiteindelijk Fabian geworden. Vervolgens zijn er 
teamafspraken gemaakt. Deze week kregen we ook onze doelgroep te weten en hebben we als team
een SWOT gemaakt en een hele hoop deskresearch gedaan naar onze doelgroep.

## Week 2
Deze week heb ik met enige hulp van Rick en Fabian een enquete in elkaar gezet en die
ook meteen online gezet. We hebben het met z'n vieren op onze facebook gedeeld. Er zijn een
paar concepten tot stand gekomen, maar na feedback erop gevraagd te hebben bleken het niet echt
goede concepten te zijn. Terug naar stap 1 dus. Ook moest
deze week een Debrief en een Merkanalyse gemaakt worden. We besloten dat Rick en ik de Debrief
gingen maken en Joshua en Fabian het Merkanalyse. 

## Week 3
Deze week moesten we een low-fid prototype maken, want Harmen van Fabrique komt volgende week
langs en dan moet er gepitcht worden. Ik heb al tegen mijn groepje gezegd dat ik wil gaan 
pitchen. We zijn op het idee gekomen een soort Tinder app te maken waarbij je feesten swiped
die je wellicht leuk vindt of niet leuk vindt. We hebben met z'n alle een papierprototype in 
elkaar gezet en zijn alvast gaan denken over een pitch.

## Week 4
Ik heb deze week mijn pitch gemaakt en heb besloten die niet op te schrijven. Ik denk
dat die dan "natuurlijker" klinkt. Na de pitch met m'n groepje een paar keer geoefend te hebben
(met positieve feedback van ze) heb ik gepitchd voor Harmen van Fabrique. Ondanks dat ons concept 
bijna met de grond gelijk werd gemaakt, waren ze wel heel positief over hoe ik aan het pitchen was.
Wederom opnieuw naar stap 1 met een nieuw concept. Dit keer geen app verzinnen, want het is moeilijk 
je doelgroep een app te laten downloaden.En het deel van ons concept met kortingen was wel goed, alleen
op zichzelf nooit genoeg.

## Week 5
De week begon stroef. We wisten even geen raad meer met wat we moesten doen. Uiteindelijk besloten we 
met z'n alle rond de tafel te gaan zitten en even heel goed na te denken over de trends van nu binnen
onze doelgroep en we hebben een hele hoop mind maps gemaakt. Toen kwam ik met het idee om de Juke Box
een nieuw leven in te blazen. Voordat we een hele hoop gingen maken, zijn we naar Elske en Robin gegaan
voor feedback (want we wilden niet weer een afgekeurd concept hebben) en die waren er heel erg positief over.
Elske heeft nog een paar hele handige tips meegegeven, zoals een "scharrel" element in het concept.

## Week 6
Deze week hebben we alles wat we in de week ervoor bedacht hebben duidelijk opgeschreven en hebben we
besloten wat we in ons concept willen hebben en wat we erbuiten willen laten. Het scharrel idee blijft erin
en door middel van kleuren op de armbandjes geef je je relatiestatus bloot tijdens het feest. De weken hierna is
de kerstvakantie dus we hebben taken verdeeld. Ik moet de niet gevalideerde Debrief verbeteren en de feedback die
ik erop heb gekregen erin verwerken. We hebben het gezamenlijk afgesloten door te eten bij Vapiano. In de vakantie zijn we van plan minimaal
een keer bij elkaar te komen.

## Week 7
De expo is om de hoek en er moet nog veel gebeuren. De Design Rationale heb ik afgemaakt, maar het is helaas
niet gevalideerd. Ik ga het de week erop nog een keer proberen te valideren. Ik heb ook meteen de feedback
van de validatie verwerkt en de Design Rationale aangepast. Deze week hebben Rick en Joshua een geweldig
High Fid klikbaar prototype gemaakt en Fabian en ik hebben armbandjes gemaakt. 

## Week 8
Maandag en dinsdag was ik ziek. Dus heb de een na laatste studiodag gemist. Wel heb ik thuis aan een pitch
gewerkt thuis. Eenmaal op school woensdag heb ik mijn pitch voorgelegd aan de rest van mijn groepje en die 
waren er positief over. We hadden onze tafel ruim voordat de expo begon al klaar gezet. De expo ging onwijs goed.
Elke docent waarvoor ik gepitcht heb was onwijs enthousiast en de ouders waarvoor ik gepresenteerd heb ook. Daarnaast
zijn de mensen van Fabrique niet bij iedereen wezen kijken, maar wel naar die van ons. Dus dat gaf mij ook een trots gevoel.
Het project is afgesloten. Nu nog het leerdossier.