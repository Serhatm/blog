---
Title: Kwartaal 3
Subtitle: Blog
Date: 2018-30-03
Tags: ["Blog", "Kwartaal3"]
Blog: OP3 Team Cactus
---

## Week 2
### 12 & 14 februari 2018
Maandag zijn de nieuwe teams zijn opgesteld. Dit keer mochten we niet zelf de teams aanmaken, maar dat hebben Elske en Bob gedaan. 
Ik was eerst heel sceptisch over het idee dat we niet zelf de teams mochten maken, maar nadat bekend werd wel team ik had was ik er alleen maar blij mee. 
Mijn team zou bestaan uit Lotte Huiskamp, Rowie Driessen en Jordan Valentijn. We zijn gelijk gaan zitten om een teamnaam te bedenken. 
Ik kwam met het idee Team Cactus, want een Cactus is gevaarlijk van buiten, maar super goed van binnen om het zo maar even te zeggen.
. Ook schetste ik snel een kleine cactus en dit sloeg snel aan bij de rest van mijn team. 
Woensdag zijn we begonnen aan de plan van aanpak van kwartaal 3 die gemaakt moest worden.
We hebben een opzet er voor gemaakt en daarna zijn we gaan kijken wat een plan van aanpak allemaal moest bevatten. 
Na info te hebben verzameld bij onder andere andere groepjes en de docenten hebben we er een gemaakt. 

## Week 3
### 19 & 21 februari 2018
Maandag was het eerste validatie moment van de plan van aanpak, deze werd niet goed gevalideerd.
We zijn daarna meteen aan de slag gegaan om de feedback te begrijpen om het vervolgens te verwerken. 
Woensdag hebben we het opnieuw gevalideerd en op een paar puntjes na werd het positief gevalideerd. 
In de middag heb ik de workshop Onderzoeksmethoden bijgewoond. 

## Week 4
### 5 & 7 maart 2018
Deze week zijn we als team alleen maar bezig geweest met de lifestyle diary. Dit kostte extra werk dan verwacht, 
omdat we eerst dachten dat het in teamverband moest, maar het bleek individueel gedaan te moeten worden.
Ik snapte er in de eerste instantie niet heel veel van, maar na het aan een paar klasgenoten binnen de studio te hebben gevraagd, 
werd het voor mij beetje bij beetje duidelijk wat de bedoeling was. 
Woensdag heb ik hier de puntjes van op de i gezet en vervolgens geprobeerd te valideren. 
Dit is niet gelukt, maar heb wel hele goede feedback erop gekregen. Als team liepen we heel erg achter op het project, 
omdat de lifestyle diary zwaarder viel dan verwacht. Verder heb ik in de middag de workshop Deskresearch bijgewoond. 
Deze was heel interessant, want ik leerde een hoop tip en tricks om het optimale uit m'n Google search fucntie te halen.

## Week 5
### 12 & 14 maart 2018
Deze week hebben we met z'n alle individueel deskresearch gedaan. Vervolgens kwamen Jordan en ik bij het besluit om interviews 
af te leggen over mediaprikkels en gezonde lifestyle. 
Na samen een paar vragen opgesteld te hebben, hebben we één voor één studenten meegenomen naar de gang om ze vervolgens te interviewen. 
Ik stelde alle vragen en Jordan die notuleerde het. Hierna zijn we begonnen om alles samen te vatten en een insight Card te maken.

## Week 6
### 19 & 21 maart 2018
Op de maandag was er geen geplande studiodag, maar we hebben als team besloten toch naar school te komen, 
omdat we redelijk achterliepen. Ik heb geholpen een begin te maken aan de ontwerpcriteria, 
maar omdat ik vroeg moest werken ben ik weg gegaan en heeft mijn team de ontwerpcriteria verder afgemaakt. 
Woensdag voelde ik me niet lekker en ben ik thuis gebleven. 

## Week 7
### 26 & 28 maart 2018
Deze week samen met Jordan de recap afgemaakt. Verder heb ik een low-fid paper prototype gemaakt samen met 
Rowie en we zijn gelijk als team wezen testen. Feedback hierop was zeer positief, wat mij veel zelfvertrouwen gaf. 
De rest van de week alleen maar aan het leerdossier en het Design Theory tentamen gezeten. Ook een klein beetje geoefend voor Tekenen Intermediate.